/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package fair.demo.dg.fulfilmentprocess.test.actions;

import fair.demo.dg.fulfilmentprocess.actions.order.ScheduleForCleanUpAction;


/**
 * Test counterpart for {@link ScheduleForCleanUpAction}
 */
public class ScheduleForCleanUp extends TestActionTemp
{
	//EMPTY
}
