/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package fair.demo.dg.fulfilmentprocess.test.actions;

/**
 * Test counterpart for
 * {@link fair.demo.dg.fulfilmentprocess.actions.order.PrepareOrderForManualCheckAction}
 */
public class PrepareOrderForManualCheck extends TestActionTemp
{
	//EMPTY
}
