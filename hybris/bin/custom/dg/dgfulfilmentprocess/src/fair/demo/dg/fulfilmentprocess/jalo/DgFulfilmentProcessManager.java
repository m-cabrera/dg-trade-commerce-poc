/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package fair.demo.dg.fulfilmentprocess.jalo;

import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import fair.demo.dg.fulfilmentprocess.constants.DgFulfilmentProcessConstants;

public class DgFulfilmentProcessManager extends GeneratedDgFulfilmentProcessManager
{
	public static final DgFulfilmentProcessManager getInstance()
	{
		ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (DgFulfilmentProcessManager) em.getExtension(DgFulfilmentProcessConstants.EXTENSIONNAME);
	}
	
}
