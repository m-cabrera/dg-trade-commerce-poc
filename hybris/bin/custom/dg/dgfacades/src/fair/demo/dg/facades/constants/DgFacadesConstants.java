/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package fair.demo.dg.facades.constants;

/**
 * Global class for all DgFacades constants.
 */
public class DgFacadesConstants extends GeneratedDgFacadesConstants
{
	public static final String EXTENSIONNAME = "dgfacades";

	private DgFacadesConstants()
	{
		//empty
	}
}
