/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at 11 Oct. 2019, 9:24:41 am                    ---
 * ----------------------------------------------------------------
 */
package fair.demo.dg.facades.constants;

/**
 * @deprecated since ages - use constants in Model classes instead
 */
@Deprecated
@SuppressWarnings({"unused","cast"})
public class GeneratedDgFacadesConstants
{
	public static final String EXTENSIONNAME = "dgfacades";
	
	protected GeneratedDgFacadesConstants()
	{
		// private constructor
	}
	
	
}
