/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package fair.demo.dg.core.jalo;

import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import fair.demo.dg.core.constants.DgCoreConstants;
import fair.demo.dg.core.setup.CoreSystemSetup;


/**
 * Do not use, please use {@link CoreSystemSetup} instead.
 * 
 */
public class DgCoreManager extends GeneratedDgCoreManager
{
	public static final DgCoreManager getInstance()
	{
		final ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (DgCoreManager) em.getExtension(DgCoreConstants.EXTENSIONNAME);
	}
}
