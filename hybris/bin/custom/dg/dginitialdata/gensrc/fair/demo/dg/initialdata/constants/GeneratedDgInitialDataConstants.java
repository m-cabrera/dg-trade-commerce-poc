/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at 11 Oct. 2019, 9:24:41 am                    ---
 * ----------------------------------------------------------------
 */
package fair.demo.dg.initialdata.constants;

/**
 * @deprecated since ages - use constants in Model classes instead
 */
@Deprecated
@SuppressWarnings({"unused","cast"})
public class GeneratedDgInitialDataConstants
{
	public static final String EXTENSIONNAME = "dginitialdata";
	
	protected GeneratedDgInitialDataConstants()
	{
		// private constructor
	}
	
	
}
