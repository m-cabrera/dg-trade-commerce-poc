/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package fair.demo.dg.initialdata.constants;

/**
 * Global class for all DgInitialData constants.
 */
public final class DgInitialDataConstants extends GeneratedDgInitialDataConstants
{
	public static final String EXTENSIONNAME = "dginitialdata";

	private DgInitialDataConstants()
	{
		//empty
	}
}
