/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at 11 Oct. 2019, 9:24:41 am                    ---
 * ----------------------------------------------------------------
 */
package org.dgbackoffice.constants;

/**
 * @deprecated since ages - use constants in Model classes instead
 */
@Deprecated
@SuppressWarnings({"unused","cast"})
public class GeneratedYacceleratorbackofficeConstants
{
	public static final String EXTENSIONNAME = "dgbackoffice";
	
	protected GeneratedYacceleratorbackofficeConstants()
	{
		// private constructor
	}
	
	
}
